#include <iostream>
#include <weather_analysis.hpp>

using namespace std;

//:: scope resolution operator
void WeatherAnalysis::loadValues() {
	cout << ".. In loadValues .." << endl;
	string fileName;
	string line;
	fileName = "../resources/Environmental_Data_Deep_Moor_2012.txt";
	time_t dateTime;
	
	fstream weatherFile(fileName, ios::in); //iostream::write
	
	while(getline(weatherFile, line)) {
		string date, timeVal;
		double temperature, pressure;
		
		stringstream lineStream(line);
		lineStream >> date >> timeVal >> temperature >> pressure;
		
		/*cout << "date:" << date << endl;
		cout << "time:" << time << endl;
		cout << "temperature:" << temperature << endl;
		cout << "pressure:" << pressure << endl;*/
		
		dateTime = convertDateTime(date, timeVal);
		barPressure[dateTime] = pressure;
		cout << "dateTime= " << dateTime << ", pressure" << pressure << endl;
		break;
	}
	
	weatherFile.close();
}

time_t WeatherAnalysis::convertDateTime(string date,string time) {

 	int yy, mm, dd, hh, min, ss;
 	time_t timeFromEpoch;
 	sscanf(date.c_str(), "%d_%d_%d", &yy, &mm, &dd);
 	sscanf(time.c_str(), "%d:%d:%d", &hh, &min, &ss);
 	
 	struct tm timeValue;
 	
 	timeValue.tm_year = yy - 1900;
 	timeValue.tm_mon = mm - 1;
 	timeValue.tm_mday = dd;
 	timeValue.tm_hour = hh;
 	timeValue.tm_min = min;
 	timeValue.tm_sec = ss;
 	
 	timeFromEpoch = mktime(&timeValue);
 	
 	return timeFromEpoch;
}
