#include <iostream>
#include <weather_analysis.hpp>

using namespace std;

int main() {
	WeatherAnalysis objWeatherAnalysis;
    string startDate, startTime, endDate, endTime;
    int coeff;

    cout << "Starting weather analysis..." << endl;

    cout << "Enter start date (yyyy_mm_dd):" << endl;
    cin >> startDate;
    cout << "Enter start time (HH:MM:SS):" << endl;
    cin >> startTime;

    cout << "Enter end date (yyyy_mm_dd):" << endl;
    cin >> endDate;
    cout << "Enter end time (HH:MM:SS):" << endl;
    cin >> endTime;

    cout << "startDate= " << startDate << " startTime= " << startTime << " endDate= " << endDate << " endTime= " << endTime << endl;
    
    coeff = objWeatherAnalysis.findPressureCoefficient(startDate, startTime, endDate, endTime);
    
    if(coeff < 0)
    	cout << "Storm coming up" << endl;
    else 
    	cout << "sunny climate" << endl;
    	
    
    objWeatherAnalysis.loadValues();

    return 0;
}
