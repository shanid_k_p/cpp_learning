#include <iostream>
#include <firmware_version.hpp>

using namespace std;

int main() {
	FirmwareVersion objFirmwareVersion;
	string currVersion, newVersion;
	
	cout << "Enter current firmware version (X.Y.Z):" << endl;
    cin >> currVersion;
    objFirmwareVersion.f_version = currVersion;

    cout << "Enter new firmware version (X.Y.Z):" << endl;
    cin >> newVersion;
    
    if(currVersion.compare(newVersion) < 0) {
    	cout << "Updating firmware version..." << endl;
    	objFirmwareVersion.updateVersion(newVersion);
    }
    else
    	cout << "Firmware already the newest version." << endl;
    	
    cout << "Current firmware version is:" << objFirmwareVersion.f_version << endl;

    return 0;
}
