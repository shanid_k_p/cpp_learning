#include <iostream>
#include <covid19_recovery.hpp>

using namespace std;

void Covid19Recovery::printValues(string startDate, string endDate) {
	time_t startDateTime, endDateTime;
	char buffer[80];
	startDateTime = convertDateTime(startDate);
	endDateTime = convertDateTime(endDate);
	
	map<time_t, int>::iterator start = recovery.lower_bound(startDateTime);
	map<time_t, int>::iterator end = recovery.upper_bound(endDateTime);
	
	cout << "date\t" << "\tcount" <<endl;
	while(start != end) {
		strftime(buffer, 80, "%D", localtime(&start->first));
		cout << buffer << "\t" << start->second << endl;
		start++;
	}
}

void Covid19Recovery::loadValues(string countryName) {
	cout << ".. In loadValues .." << endl;
	string fileName;
	string line, lineAll, date, count, country;
	fileName = "../resources/time_series_covid19_recovered_global.csv";
	
	fstream rateFile(fileName, ios::in); //iostream::write
	
	//search for country and load data
	while(getline(rateFile, lineAll)) {
		char c;
		country = "";
		stringstream ssCountry(lineAll);
		ssCountry.ignore(256, ',');
		while(ssCountry.peek() != ',') {
			ssCountry.get(c);
			country.push_back(c);
		}
		if(country.compare(countryName) == 0) {
			//fstream to read date
			fstream rateFile2(fileName, ios::in);
			getline(rateFile2, line);
			stringstream ssDate(line);
			ssDate.ignore(256, ',').ignore(256, ',').ignore(256, ',').ignore(256, ',');
			ssCountry.ignore(256, ',').ignore(256, ',').ignore(256, ',');
			
			while(getline(ssCountry, count, ',') && getline(ssDate, date, ',')) 
				recovery[convertDateTime(date)] = stoi(count);
			
			rateFile2.close();
			break;
		}
	}
	
	rateFile.close();
}

time_t Covid19Recovery::convertDateTime(string date) {

 	int yy, mm, dd;
 	time_t timeFromEpoch;
 	sscanf(date.c_str(), "%d/%d/%d", &mm, &dd, &yy);
 	//sscanf(time.c_str(), "%d:%d:%d", &hh, &min, &ss);
 	
 	struct tm timeValue;
 	
 	timeValue.tm_year = yy + 2000 - 1900;
 	timeValue.tm_mon = mm - 1;
 	timeValue.tm_mday = dd;
 	timeValue.tm_hour = 0;
 	timeValue.tm_min = 0;
 	timeValue.tm_sec = 0;
 	
 	timeFromEpoch = mktime(&timeValue);
 	
 	return timeFromEpoch;
}
