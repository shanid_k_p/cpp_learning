#include <iostream>
#include <covid19_recovery.hpp>

using namespace std;

int main(int argc, char** argv) {
	Covid19Recovery objCovid19Recovery;
    string startDate, endDate, countryName="";
    string whitespace=" ";
    
    if(argc < 4) {
    	cout << "run with arguments <countryName> <startDate mm/dd/yy> <endDate mm/dd/yy>" << endl;
    	exit(1);
    }
    else if(argc > 4) {
    	startDate = argv[argc-2];
    	endDate = argv[argc-1];
    	
    	for(int i=1; i< argc-2; i++)
    		countryName.append(whitespace+argv[i]);
    	countryName.erase(0, 1);
    }
    else {
    	countryName = argv[1];
    	startDate = argv[2];
    	endDate = argv[3];
    }

    cout << "country Name:" << countryName << endl;
    cout << "start Date:" << startDate << endl;
    cout << "end Date:" << endDate << endl;

    objCovid19Recovery.loadValues(countryName);
    objCovid19Recovery.printValues(startDate, endDate);

    return 0;
}
