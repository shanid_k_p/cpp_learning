#include <map>
#include <ctime>
#include <fstream>
#include <sstream>

using namespace std;

class Covid19Recovery{
public:
	map <time_t, int> recovery;
	void loadValues(string);
	void printValues(string, string);
	time_t convertDateTime(string);
};
